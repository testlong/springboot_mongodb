package com;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by laiguanglong on 2017/11/7.
 */
public interface UserRepository extends MongoRepository<User, Long> {

    User findByUsername(String username);

}